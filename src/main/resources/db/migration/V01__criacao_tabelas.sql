CREATE TABLE public.score_credito (
  score_credito_id SERIAL NOT NULL,
  cpf TEXT,
  nascimento date,
  endereco pg_catalog.jsonb,
  fonte_renda pg_catalog.jsonb,
  bens pg_catalog.jsonb,
  PRIMARY KEY(score_credito_id)
) ;