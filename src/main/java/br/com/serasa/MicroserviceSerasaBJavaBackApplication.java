package br.com.serasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class MicroserviceSerasaBJavaBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceSerasaBJavaBackApplication.class, args);
    }

}
