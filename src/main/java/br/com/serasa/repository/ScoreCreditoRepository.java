package br.com.serasa.repository;

import br.com.serasa.model.ScoreCredito;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScoreCreditoRepository extends JpaRepository<ScoreCredito, Integer> {

}
