package br.com.serasa.resource;

import br.com.serasa.event.RecursoCriadoEvent;
import br.com.serasa.model.ScoreCredito;
import br.com.serasa.service.ScoreCreditoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
@RequestMapping("/scorecredito")
public class ScoreCreditoResource {

	@Autowired
	private ApplicationEventPublisher publisher;
	@Autowired
	private ScoreCreditoService scoreCreditoService;

	@CrossOrigin
	@GetMapping
	public List<ScoreCredito> listar() {
		return scoreCreditoService.listarTodos();
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<ScoreCredito> cria(@Valid @RequestBody ScoreCredito scoreCredito, HttpServletResponse response) throws NoSuchAlgorithmException {
		ScoreCredito scoreCreditoSalvo = scoreCreditoService.salvar(scoreCredito);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, scoreCreditoSalvo.getScoreCreditoId()));

		return ResponseEntity.status(HttpStatus.CREATED).body(scoreCreditoSalvo);
	}

	@CrossOrigin
	@PutMapping("{codigo}")
	public ResponseEntity<ScoreCredito> atualizar(@PathVariable Integer codigo, @RequestBody ScoreCredito scoreCredito) {

		return ResponseEntity.ok(scoreCreditoService.atualizar(codigo, scoreCredito));
	}

	@CrossOrigin
	@GetMapping("/{codigo}")
	public ResponseEntity<ScoreCredito> buscarPeloCodigo(@PathVariable Integer codigo, HttpServletResponse response) {
		ScoreCredito scoreCredito = scoreCreditoService.buscarScoreCreditoPeloCodigo(codigo);

		if (scoreCredito == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(scoreCredito);
	}

	@CrossOrigin
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Integer codigo) {
		scoreCreditoService.deletar(codigo);
	}
	
	@CrossOrigin
	public List<ScoreCredito> listarTodos() {	
		return scoreCreditoService.listarTodos();
	}

}