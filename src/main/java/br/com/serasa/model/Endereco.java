package br.com.serasa.model;

import java.io.Serializable;
import java.util.Objects;

public class Endereco implements Serializable {

    private String endereco;
    private String cep;
    private String numero;
    private String cidade;
    private String estado;

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Endereco)) return false;
        Endereco endereco1 = (Endereco) o;
        return Objects.equals(getEndereco(), endereco1.getEndereco()) &&
                Objects.equals(getCep(), endereco1.getCep()) &&
                Objects.equals(getNumero(), endereco1.getNumero()) &&
                Objects.equals(getCidade(), endereco1.getCidade()) &&
                Objects.equals(getEstado(), endereco1.getEstado());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEndereco(), getCep(), getNumero(), getCidade(), getEstado());
    }
}
