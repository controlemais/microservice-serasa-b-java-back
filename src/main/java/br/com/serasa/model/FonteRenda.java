package br.com.serasa.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class FonteRenda implements Serializable {
    private String descricao;
    private BigDecimal valor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FonteRenda)) return false;
        FonteRenda that = (FonteRenda) o;
        return Objects.equals(getDescricao(), that.getDescricao()) &&
                Objects.equals(getValor(), that.getValor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDescricao(), getValor());
    }
}
