package br.com.serasa.model;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "score_credito")
@SequenceGenerator(name = "score_credito_score_credito_id_seq", sequenceName = "score_credito_score_credito_id_seq", allocationSize = 1)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class ScoreCredito implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "score_credito_score_credito_id_seq")
    private Integer scoreCreditoId;
    @NotNull
    private String cpf;
    private LocalDate nascimento;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Endereco endereco;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private FonteRenda fonteRenda;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<Bem> bens;

    public Integer getScoreCreditoId() {
        return scoreCreditoId;
    }

    public void setScoreCreditoId(Integer scoreCreditoId) {
        this.scoreCreditoId = scoreCreditoId;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public FonteRenda getFonteRenda() {
        return fonteRenda;
    }

    public void setFonteRenda(FonteRenda fonteRenda) {
        this.fonteRenda = fonteRenda;
    }

    public List<Bem> getBens() {
        return bens;
    }

    public void setBens(List<Bem> bens) {
        this.bens = bens;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScoreCredito)) return false;
        ScoreCredito that = (ScoreCredito) o;
        return Objects.equals(getScoreCreditoId(), that.getScoreCreditoId()) &&
                Objects.equals(getCpf(), that.getCpf()) &&
                Objects.equals(getNascimento(), that.getNascimento()) &&
                Objects.equals(getEndereco(), that.getEndereco()) &&
                Objects.equals(getFonteRenda(), that.getFonteRenda()) &&
                Objects.equals(getBens(), that.getBens());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getScoreCreditoId(), getCpf(), getNascimento(), getEndereco(), getFonteRenda(), getBens());
    }
}
