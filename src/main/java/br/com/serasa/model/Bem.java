package br.com.serasa.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class Bem implements Serializable {
    private String descricao;
    private BigDecimal valor;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bem)) return false;
        Bem bem = (Bem) o;
        return Objects.equals(getDescricao(), bem.getDescricao()) &&
                Objects.equals(getValor(), bem.getValor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDescricao(), getValor());
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
