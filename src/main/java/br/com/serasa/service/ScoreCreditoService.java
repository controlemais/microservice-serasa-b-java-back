package br.com.serasa.service;

import br.com.serasa.model.ScoreCredito;
import br.com.serasa.repository.ScoreCreditoRepository;
import br.com.serasa.util.Constantes;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class ScoreCreditoService {

	@Autowired
	private ScoreCreditoRepository scoreCreditoRepository;

	@CacheEvict(cacheNames = Constantes.SCORE_CREDITO, allEntries = true)
	public ScoreCredito salvar(ScoreCredito scoreCredito) throws NoSuchAlgorithmException {
		return scoreCreditoRepository.save(scoreCredito);
	}

	@CachePut(cacheNames = Constantes.SCORE_CREDITO, key="#scoreCredito.getScoreCreditoId()")
	public ScoreCredito atualizar(Integer codigo, ScoreCredito scoreCredito) {
		ScoreCredito scoreCreditoBanco = buscarScoreCreditoPeloCodigo(codigo);
		BeanUtils.copyProperties(scoreCredito, scoreCreditoBanco, "scoreCreditoId");
		return scoreCreditoRepository.save(scoreCreditoBanco);
	}

	@Cacheable(cacheNames = Constantes.SCORE_CREDITO, key="#scoreCreditoId")
	public ScoreCredito buscarScoreCreditoPeloCodigo(Integer codigo) {
		ScoreCredito scoreCreditoBanco = scoreCreditoRepository.findById(codigo).orElse(null);
		if (scoreCreditoBanco == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return scoreCreditoBanco;
	}

	@Cacheable(cacheNames = Constantes.SCORE_CREDITO, key="#root.method.name")
	public List<ScoreCredito> listarTodos() {
		return scoreCreditoRepository.findAll();
	}

	@CacheEvict(cacheNames = Constantes.SCORE_CREDITO, key="#scoreCreditoId")
	public void deletar(Integer codigo) {
		scoreCreditoRepository.deleteById(codigo);
	}

}
