#Projeto Microservice Base B

Primeiramente temos que rodar o comando para criar e subir o banco de dados **postgresql**.

`docker-compose -f postgresql.yml up`

Como este projeto utiliza Redis como servidor de cache, também é necessário subir a imagem do mesmo.

`docker-compose -f redis.yml up`
  
Agora podemos executar o projeto spring boot pois o mesmo ira criar as tabelas necessárias para o sistema funcionar.